# JDA Assignment Image Crawler

There are 2 implementations

> Using Beautiful soup and another using scrapy

> url_list.txt contains list of sites.

-----1st Implementation ------

> python jda_bs.py url_list.txt

Files downloaded in images directory

-----2nd Implementation ------

Using Scrapy

> Installation

          pip install -r requirements.txt


> You can simply run scrapy like this
scrapy crawl jda -a start_url=https://www.rediff.com


> Run Program to crawl file with site_urls


         sh run.sh <filename>

         example

         sh run.sh url_list.txt


You can find the image files in images directory under sub directory full

