import scrapy
from scrapy.crawler import CrawlerProcess,CrawlerRunner
from jda_spider import JDASpider
import os,uuid,sys,warnings
import scrapy.crawler as crawler,time

from scrapy.utils.project import get_project_settings
settings = get_project_settings()
from twisted.internet import reactor
from importlib import import_module
from multiprocessing import Process, Queue

warnings.filterwarnings('ignore')

class JDAS(object):

    def call(self,start_url):
        process = CrawlerProcess(settings)
        process.crawl('jda',start_url=start_url)
        process.start()
        time.sleep(0.5)
        os.execl(sys.executable, sys.executable, *sys.argv)

def run_spider(start_url):
    def f(q):
        # try:
        runner = crawler.CrawlerRunner()
        deferred = runner.crawl(JDASpider,start_url=start_url)
        deferred.addBoth(lambda _: reactor.stop())
        reactor.run()
        q.put(None)
        # except Exception as e:
        #     q.put(e)

    q = Queue()
    p = Process(target=f, args=(q,))
    p.start()
    result = q.get()
    p.join()

    if result is not None:
        raise result

def run_spider1(start_url):
    module_name="jda.spiders.{}".format("jda_spider")
    scrapy_var = import_module(module_name)   #do some dynamic import of selected spider
    spiderObj=JDASpider           #get mySpider-object from spider module
    crawler = CrawlerRunner(settings)   #from Scrapy docs
    crawler.crawl(JDASpider,start_url=start_url)

if __name__ == "__main__":

    with open(sys.argv[1]) as crawl_urls_file:
        for i in crawl_urls_file:
            images = JDAS(i)


