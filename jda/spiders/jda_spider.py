import scrapy
#from scrapy.contrib.linkextractors.htmlparser import HtmlLinkExtractor
from scrapy.linkextractors.lxmlhtml import LxmlLinkExtractor
from scrapy.linkextractors.sgml import  SgmlLinkExtractor
from scrapy.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
import urllib2,requests
from scrapy.utils.project import get_project_settings
settings = get_project_settings()

from requests.auth import HTTPBasicAuth
import urllib2,requests,os,sys,re
from urlparse import urlparse
from jda.items import JDAItem
from urlparse import urljoin
import requests
from io import BytesIO
from scrapy import Request
import time,datetime,warnings,json,base64,cStringIO,urllib
warnings.filterwarnings('ignore')
from scrapy.loader import ItemLoader

class JDASpider(CrawlSpider):
    name = "jda"
    
    def __init__(self,domain='',*args,**kwargs):

        super(CrawlSpider, self).__init__(*args, **kwargs)
        start_url = kwargs.get('start_url')
        self.start_urls = [start_url]
        global start_urls,rules
        #settings.set('DEPTH_LIMIT',1)
        settings.set('ROBOTSTXT_OBEY',True)

        self.start_urls = [start_url]
        start_urls = self.start_url
        self.first = 0

        self.rules = (Rule(LxmlLinkExtractor(allow_domains = [self.extract_domain(start_url)]), follow=True),)
        rules = self.rules
        self._rules = rules

        JDASpider.start_urls = [start_url]

    def extract_domain(self,url):
        return urlparse(url).netloc


    def parse(self, response):
        image = JDAItem()
        rel_paths = response.xpath("//img/@src").extract()
        image['image_urls'] = []
        for rel in rel_paths:
            rel = rel if rel.startswith('http')  else 'https:'+rel
            yield JDAItem(image_urls=[rel],image=rel)
        for href in response.xpath('//a/@href').getall():
            yield scrapy.Request(response.urljoin(href), self.parse)





