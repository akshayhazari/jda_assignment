# Scrape article main img
import bs4,urllib2,time,sys,re,logging

logging.basicConfig(filename="bs.log",
                    format='%(asctime)s %(message)s',
                    filemode='w')
logger=logging.getLogger()
logger.setLevel(logging.ERROR)

def get_images(url):
    timestamp = time.asctime()

    # Parse HTML of article, aka making soup
    soup = bs4.BeautifulSoup(urllib2.urlopen(url).read())

    # Create a new file to write content to
    txt = open('images/%s.jpg' % timestamp, "wb")
    urls = soup.find_all('img', src=True)
    for image in urls:
        img_name = image['src']
        filename = img_name[img_name.rfind("/") + 1:]
        try:
            txt = open('images/%s.jpg' % filename, "wb")
            image = image["src"].split("src=")[-1]
            download_img = urllib2.urlopen(image)
            txt.write(download_img.read())
            txt.close()
        except Exception, e:
            logger.error("Unable to get image file {} : {}".format(filename,e))

if __name__ == "__main__":
    if len(sys.argv)>1:
        filename = sys.argv[1]
    else:
        print "Please provide filename"

    with open(filename) as url_list:
        for i in url_list:
            print i
            get_images(i)

